using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "c43cd97daf62ccb87ffca8b7e15b6eaaeac54e01")]
public class BirdCam : Component
{
	private PlayerDummy camera;
	public vec3 offset;
	public Node playerTarget = null;
	

	private Input.MOUSE_BUTTON mouseRightKey = Input.MOUSE_BUTTON.RIGHT;

	[HideInEditor]
	public vec3 mouseHitPos = new vec3 (0,5,0);

	[HideInEditor]
	public Node Target = null;

	public NavScript navScript;

	void Init()
	{
		Visualizer.Enabled = true;
		// get the camera from the current node
		camera = node as PlayerDummy;
	}

	void Update()
	{
		node.WorldPosition = new vec3(playerTarget.Position.x + offset.x, playerTarget.Position.y + offset.y, playerTarget.Position.z + offset.z);
		ivec2 mouse = Input.MouseCoord;
		float length = 500.0f;
		vec3 start = node.Position;
		vec3 end = start + new vec3(camera.GetDirectionFromScreen(mouse.x, mouse.y)) * length;

		// ignore surfaces that have certain bits of the Intersection mask enabled
		//int mask = ~(1 << 2 | 1 << 4);
		int mask = ~(0);

		WorldIntersectionNormal intersection = new WorldIntersectionNormal();

		Unigine.Object obj = World.GetIntersection(start, end, mask, intersection);

		if (obj)
		{
			vec3 point = intersection.Point;
			vec3 normal = intersection.Normal;
			if (Input.IsMouseButtonPressed(mouseRightKey))
			{
				navScript.canMove = true;
				navScript.targetPos = new vec3(point.x, point.y, 0.5f);
			}
		}
	}
}