using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "cfde541596469670aee5370fd279b92280b2e619")]
public class NavScript : Component
{
	public float speed = 4.0f;
	public NavigationMesh navigationMesh = null;

	[ParameterFile(Filter = ".node")]
	public string targetReferencePath = "";

	public bool visualizeRoute = false;

	private float bounds = 9.5f;

	public BirdCam BirdCam;
	private int mask = ~(5);
	private List<Unigine.Object> obj = new List<Unigine.Object>();
	private List<WorldIntersectionNormal> intersections = new List<WorldIntersectionNormal>();
	private WorldIntersectionNormal intersection = new WorldIntersectionNormal();
	private WorldIntersectionNormal intersection2 = new WorldIntersectionNormal();
	private WorldIntersectionNormal intersection3 = new WorldIntersectionNormal();
	private WorldIntersectionNormal intersection4 = new WorldIntersectionNormal();

	private List <float> dist1 = new List <float>();
	private float lowestDist = 0f;
	private int lowestIndex = 0;

	private float radiusFactor = 2.5f;

	public float length = 25f;
	private float abnormalAmount = 99999999f;
	[ParameterColor]
	public vec4 routeColor = vec4.ZERO;

	private PathRoute route = null;
	[HideInEditor]
	public Node target = null;
	[HideInEditor]
	public vec3 targetPos = vec3.ZERO;

	[HideInEditor]
	public bool canMove = false;

	private void Init()
	{
		if (!navigationMesh)
		{
			return;
		}

		if (!target)
		{
			// create target
			target = World.LoadNode(targetReferencePath);
		}

		// create route to path calculation
		route = new PathRoute();

		// set point radius inside navigation mesh
		route.Radius = 0.2f;

		// does not use negative speed
		speed = MathLib.Max(0.0f, speed);

		// enabled for visualization
		if (visualizeRoute)
		{
			Visualizer.Enabled = true;
		}
	}
	
	private void Update()
	{
		if (!navigationMesh || !target)
		{
			return;
		}

		// stops movement if close to target
		if ((target.WorldPosition - node.WorldPosition).Length < 0.51f)
		{
			canMove = false;
		}

		// if current path is ready, try to move node
		if (route.IsReady)
		{
			// in successful case change direction and position
			if (route.IsReached)
			{
				// enable target from inside obstacle
				if (!target.Enabled)
				{
					//target.Enabled = true;
				}
				// get new direction for node
				vec3 direction = route.GetPoint(1) - route.GetPoint(0);
				if (direction.Length2 > MathLib.EPSILON && canMove)
				{
					// get rotation target based on new direction
					quat targetRotation = new quat(MathLib.SetTo(vec3.ZERO, direction.Normalized, vec3.UP, MathLib.AXIS.Y));

					// smoothly change rotation
					quat currentRotation = MathLib.Slerp(node.GetWorldRotation(), targetRotation, Game.IFps * 45.0f);
					node.SetWorldRotation(currentRotation);

					// translate in forward direction and try to save node position in navigation mesh
					vec3 lastValidPosition = node.WorldPosition;
					node.Translate(vec3.FORWARD * Game.IFps * speed);

					// restore last position if node is outside navigation mesh
					if (navigationMesh.Inside2D(node.WorldPosition, route.Radius) == 0)
					{
						node.WorldPosition = lastValidPosition;
					}
				}
				//Log.Message("Testing Aids");
				//target.WorldPosition = targetPos;
				// render current path
				if (visualizeRoute)
				{
					route.RenderVisualizer(routeColor);
				}
				// try to create new path
				route.Create2D(node.WorldPosition + vec3.UP * 0.5f, target.WorldPosition, 1);
			}
			else
			{
				vec3 tempPos = new vec3(targetPos.x, targetPos.y, -0.1f);
				obj.AddRange(new Unigine.Object[] 
				{
					World.GetIntersection(tempPos, tempPos + vec3.FORWARD * length, mask, intersection), 
					World.GetIntersection(tempPos, tempPos + vec3.RIGHT * length, mask, intersection2),
					World.GetIntersection(tempPos, tempPos + vec3.BACK * length, mask, intersection3),
					World.GetIntersection(tempPos, tempPos + vec3.LEFT * length, mask, intersection4)
				});
				intersections.AddRange(new WorldIntersectionNormal[] 
				{
					intersection, 
					intersection2, 
					intersection3, 
					intersection4
				});

				for (int i = 0; i < obj.Count; i++)
				{
					if (obj[i])
					{
						if ((intersections[i].Point - tempPos).Length < lowestDist)
						{
							lowestDist = (intersections[i].Point - tempPos).Length;
							lowestIndex = i;
						}
					}
				}
				if (navigationMesh.Inside2D(target.WorldPosition, route.Radius) == 0)
				{
					if (lowestIndex == 0)
					{
						targetPos = new vec3(intersections[lowestIndex].Point.x, intersections[lowestIndex].Point.y + route.Radius*radiusFactor, 0.5f);
					}
					else if (lowestIndex == 1)
					{
						targetPos = new vec3(intersections[lowestIndex].Point.x + route.Radius*radiusFactor, intersections[lowestIndex].Point.y, 0.5f);
					}
					else if (lowestIndex == 2)
					{
						targetPos = new vec3(intersections[lowestIndex].Point.x, intersections[lowestIndex].Point.y - route.Radius*radiusFactor, 0.5f);
					}
					else if (lowestIndex == 3)
					{
						targetPos = new vec3(intersections[lowestIndex].Point.x - route.Radius*radiusFactor, intersections[lowestIndex].Point.y, 0.5f);
					}
				}
				else
				{
					if (lowestIndex == 0)
					{
						targetPos = new vec3(intersections[lowestIndex].Point.x, intersections[lowestIndex].Point.y - route.Radius*radiusFactor, 0.5f);
					}
					else if (lowestIndex == 1)
					{
						targetPos = new vec3(intersections[lowestIndex].Point.x - route.Radius*radiusFactor, intersections[lowestIndex].Point.y, 0.5f);
					}
					else if (lowestIndex == 2)
					{
						targetPos = new vec3(intersections[lowestIndex].Point.x, intersections[lowestIndex].Point.y + route.Radius*radiusFactor, 0.5f);
					}
					else if (lowestIndex == 3)
					{
						targetPos = new vec3(intersections[lowestIndex].Point.x + route.Radius*radiusFactor, intersections[lowestIndex].Point.y, 0.5f);
					}
				}
				lowestDist = abnormalAmount;
				route.Create2D(node.WorldPosition + vec3.UP * 0.5f, target.WorldPosition, 1);
			}
			target.WorldPosition = targetPos;
		}
		// try to create new path
		else if (!route.IsQueued)
		{
			route.Create2D(node.WorldPosition + vec3.UP * 0.5f, target.WorldPosition, 1);
		}
	}

	public static vec3 MoveTowards(vec3 current, vec3 target, float maxDistanceDelta)
	{
     	vec3 a = target - current;
     	float magnitude = a.Length;
     	if (magnitude <= maxDistanceDelta || magnitude == 0f)
     	{
         	return target;
     	}
     	return current + a / magnitude * maxDistanceDelta;
	}

	private void Shutdown()
	{
		Visualizer.Enabled = false;
	}
}